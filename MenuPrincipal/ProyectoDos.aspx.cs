﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MenuPrincipal
{
    public partial class ProyectoDos1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Buttonshow_Click(object sender, EventArgs e)
        {
            labelRes1.Text = Convert.ToString(this.Session["res1"]);
            labelRes2.Text = Convert.ToString(this.Session["res2"]);
            labelRes3.Text = Convert.ToString(this.Session["res3"]);

            resTotal.Text = Convert.ToString(this.Session["resTotal"]);

        }

        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("ProyectoUno.aspx");
        }
    }
}