﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MenuPrincipal
{
    public partial class ProyectoUno : System.Web.UI.Page
    {
        Decimal a, b, c;
        Decimal res;
        HttpCookie ck;
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
            this.Response.Write("<h2>HOLA MUNDO</h2");
            
            ck = this.Request.Cookies["DatoUno"];
            if (ck != null)
            {
                TextBoxDatoUno.Text = ck.Value;
            }
        }

        protected void TextBoxDatoUno_TextChanged(object sender, EventArgs e)
        {
            a = Convert.ToDecimal(TextBoxDatoUno.Text);
            this.Session["res1"] = a;
        }

        protected void TextBoxDatoDos_TextChanged(object sender, EventArgs e)
        {
            b = Convert.ToDecimal(TextBoxDatoDos.Text);
            this.Session["res2"] = b;

        }

        protected void ButtonCalcula_Click(object sender, EventArgs e)
        {
            c = Convert.ToDecimal(Request.Form["TextBoxDatoTres"]);
            this.Session["res3"] = c;

            res = Convert.ToDecimal(a + b + c);
            this.ViewState["res"] = res;
            this.Session["resTotal"] = res;

            //Se define el cookie
            ck = new HttpCookie("DatoUno", TextBoxDatoUno.Text);
            //Tiempo de vida de cookie
            ck.Expires = DateTime.Now.AddSeconds(40);
            this.Response.Cookies.Add(ck);


            LabelResult.Text = Convert.ToString("Resultado: " + res );
        }

        protected void ButtonIrDos_Click(object sender, EventArgs e)
        {
            // Mostrar ventana 2
            res = (Decimal)this.ViewState["res"];
            this.Response.Redirect("ProyectoDos.aspx?PasaDato=" + res);
        }
    }
}