﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProyectoUno.aspx.cs" Inherits="MenuPrincipal.ProyectoUno" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript">
        function Longitud(sender, args) {
            if (args.Value.length < 3) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }
    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <appSettings>
        <add key ="ValidationSetttings:UnobtrusiveValidationMode" value="None"/>
    </appSettings>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="TextBoxDatoUno" type="number" runat="server" OnTextChanged="TextBoxDatoUno_TextChanged"></asp:TextBox>
        <asp:RequiredFieldValidator ID="validadorCampo1" runat="server" ControlToValidate="TextBoxDatoUno" ErrorMessage="El campo no puede estar vacío"></asp:RequiredFieldValidator>
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:RangeValidator ID="rangoCampo1" runat="server" ErrorMessage="Valor sólo entre 1 y 100" ControlToValidate="TextBoxDatoUno" MaximumValue="100000" MinimumValue="1" Type="Integer"></asp:RangeValidator>
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:CustomValidator ID="CustomCampo1" runat="server" ErrorMessage="Maximo 3 digitos" ClientValidationFunction="Longitud" ControlToValidate="TextBoxDatoUno"></asp:CustomValidator>
        <br />
    </div>
    <div>
        <asp:TextBox ID="TextBoxDatoDos" type="number" runat="server" OnTextChanged="TextBoxDatoDos_TextChanged"></asp:TextBox>
        <asp:CompareValidator ID="comparaCampo2" runat="server" ControlToCompare="TextBoxDatoUno" ControlToValidate="TextBoxDatoDos" ErrorMessage="Dato 1 debe ser igual a dato 2" Type="Integer"></asp:CompareValidator>
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>

        <div>
        <input id="TextBoxDatoTres" type="number" runat="server"/>
            
            <br />
            
        </div>
        <div>
            <asp:Button ID="ButtonCalcula" runat="server" Text="Button" OnClick="ButtonCalcula_Click" />
            <asp:Button ID="ButtonIrDos" runat="server" OnClick="ButtonIrDos_Click" Text="Ir Pagina 2" />
            <br />
            <br />
            <asp:Label ID="LabelResult" runat="server" Text="Resultado: 0"></asp:Label>
        </div>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
        </form>
</body>
</html>
